package com.videonetics.wsdl;

import java.net.MalformedURLException;
import java.rmi.RemoteException;

import org.apache.axis2.client.ServiceClient;
import org.tempuri.VTPLWebServicesStub;
import org.tempuri.VTPLWebServicesStub.ArrayOfVChannel;
import org.tempuri.VTPLWebServicesStub.GetChannels;
import org.tempuri.VTPLWebServicesStub.GetChannelsResponse;
import org.tempuri.VTPLWebServicesStub.VChannel;
import org.tempuri.VTPLWebServicesStub.VChannelList;

/**
 * VTPLWebServicesExecutor!
 *
 */
public class VTPLWebServicesExecutor {
	
    public static void main( String[] args ) throws MalformedURLException, RemoteException {
        
        
        VTPLWebServicesStub vTPLWebServicesStub = new VTPLWebServicesStub();
        ServiceClient client = vTPLWebServicesStub._getServiceClient();
        client.engageModule("addressing");
        
        GetChannels getChannels = new GetChannels();
        getChannels.setOffset(2L);
        getChannels.setCount(2L);
        getChannels.setSessonId(2L);
        getChannels.setTimeStamp(1447940159000L);
        GetChannelsResponse getChannelsResponse = vTPLWebServicesStub.getChannels(getChannels);
        
        VChannelList vChannelList = getChannelsResponse.getGetChannelsResult();
        ArrayOfVChannel arrayOfVChannel = vChannelList.getChannelList();
        VChannel[] vChannels = arrayOfVChannel.getVChannel();
        for (VChannel vChannel : vChannels) {
        	System.out.println(vChannel.getId());
			System.out.println(vChannel.getName());
		}
    }
}
