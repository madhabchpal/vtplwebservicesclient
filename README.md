Generate the VTPLWebServices stub by SoapUI and Apache Axis2 - wsdl2java
------------------------------------------------------------------------

1.	Install SoapUI and create a "New SOAP Project"
2.	Project Name: VTPLWebServices
	Initial WSDL: http://192.168.1.150/VTPLWebServices.svc?wsdl
3.	Install and set Apache Axis2 to SoapUI Global Preferences
	SoapUI -> Preferences -> Tools -> Axis 2: C:\axis2-1.6.3
4.	Right click on the Web Service Interface: WSHttpBinding_IVTPLWebServices
	Select: Generate Code -> Axis 2 Artifacts
	Use Cached WSDL: <Tick>
	Output Directory: VTPLServicesClient\src\main\java\org\tempuri
	Package: org.tempuri
	databinding methods: adb
	sync: <Tick>
5.	Click generate. The VTPLWebServicesStub.java will be generated.
6.	Place the VTPLWebServicesStub.java in the package org.tempuri.
7.	Now we can create a service client and make http call to the web service.
	The getChannels method is called in the VTPLWebServicesExecutor.java -> main. 